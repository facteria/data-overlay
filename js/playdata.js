const playdata = (() => {
  // TODO: [Stretch] Generalise judges to work for any number of them
  const judgements = [0, 0, 0, 0, 0, 0];
  let judgeType = 'itg';
  const judgeThres = {
    itg: [115, 100, 85, 50, 30],
    ddr: [115, 107, 98, 68, 43],
    fives: [115, 110, 95, 70, 35]
  };
  const averages = {
    NoteA: {pre: 0, post: 0, dist: 0, notesPre: 0, notesPost: 0},
    NoteB: {pre: 0, post: 0, dist: 0, notesPre: 0, notesPost: 0}
  };

  const noteStage = { PRE: 1, POST: 2 };
  Object.freeze(noteStage);

  // FIXME: Remove when caching score is no longer necessary (26/1/21)
  let score = 0;
  const updateScore = (() => currentScore => { score = currentScore })();
  const numScore = (() => () => score)();

  const dataLength = (() => () => data.length)();

  // ---

  // Array of objects, by note ID, with:
  // 	 pre: Pre-cut score (70)
  // 	post: Post-cut score (30)
  // 	dist: Distance from centre score (15)
  // 	mult: Multiplier (for score tracking)
  //  type: Left, right, or mine (for individual averages)
  //  time: Time in ms since start of song (for graph drawing)
  //   dir: Direction was correct
  //  side: Saber side (left/right) was correct
  const data = [];
  const ghost = [];
  let currentGhostScore = 0;
  const getGhostScore = (() => () => currentGhostScore)();
  const ghostExists = (() => () => ghost.length > 0)();

  // Array of objects, in order of time, with:
  //  start: Song time in ms of the start of this combo
  //    end: Song time in ms combo finished. -1 means we're still in it
  //  combo: Value of the combo
  const emptyCombo = {start: -1, end: -1, combo: 0};
  const comboData = [{start: emptyCombo.start, end: emptyCombo.end, combo: emptyCombo.combo}];

  // Both arrays of values determining frequency of each bucket
  const distanceDev = [];
  const scoreHist = [];

  const setGhost = (ghostB64) => {
    ghost.length = 0;
    for (let note of b64ToData(ghostB64)) ghost.push(note);
  };

  /** Converts the note data object into a 3-byte binary representation. */
  const noteToBinary = (() => {
    const push = (current, value, dist) => current << dist | value;

    return (note) => {
      const {pre, post, dist, mult, type, dir, side} = note;
      let b1, b2, b3;
      b1 = push(0, Math.log2(mult), 2);
      b1 = push(b1, dir | 0, 1);
      b1 = push(b1, side | 0, 1);
      b1 = push(b1, type === 'NoteB' | 0, 1);
      b1 = push(b1, type === 'Bomb' | 0, 1);

      // (pre) 7654321_ <- 5---- (post)
      b2 = (pre << 1 | post >> 4) & 255;
      // (post) 5/4321____ <- 4321 (dist) (shift left 4, 255 mask takes care of bit 5)
      b3 = (post << 4 | dist) & 255;
      return [b1, b2, b3];
    }
  })();

  /** Converts a 3-byte array into a note data object. */
  const binaryToNote = (() => {
    return (arr) => {
      return {
        dist: arr[2] & 15,                      //          ----4321
        pre: arr[1] >> 1,                       // 8765432-
        post: (arr[1] & 1) << 4 | arr[2] >> 4,  // -------1 8765----
        type: (arr[0] & 1) === 1 ? 'Bomb' : ((arr[0] & 2) >> 1) === 0 ? 'NoteA' : 'NoteB',
        side: (arr[0] & 1) === 1 ? null : ((arr[0] & 4) >> 2) !== 0,
        dir: (arr[0] & 1) === 1 ? null : ((arr[0] & 8) >> 3) !== 0,
        mult: Math.pow(2, (arr[0] & 48) >> 4)
      }
    }
  })();

  const dataToB64 = (() => {
    return () => {
      let resArr = new Uint8Array(data.length * 3);
      data.forEach((el, i) => {
        const bin = noteToBinary(data[i]);
        for (let j = 0; j < bin.length; j++) {
          resArr[i * bin.length + j] = bin[j];
        }
      });
      return btoa(String.fromCharCode.apply(null, resArr));
    }
  })();

  const b64ToData = (() => {
    return (b64) => {
      let result = [];
      const bin = new Uint8Array(atob(b64).split("").map(c => c.charCodeAt(0)));
      for (let i = 0; i < bin.length / 3; i++) {
        result.push(binaryToNote(bin.subarray(i * 3, i * 3 + 3)));
      }
      return result;
    }
  })();

  /** Places the score in the appropriate bucket. */
  const bucketJudge = (() => {
    return (score) => {
      for (let i = 0; i < judgeThres[judgeType].length; i++) {
        if (score >= judgeThres[judgeType][i]) {
          judgements[i]++;
          return;
        }
      }
      judgements[judgements.length - 1]++;
    }
  })();

  const bucketDistance = dist => {
    let val = distanceDev[(dist*100).toFixed()];
    distanceDev[(dist*100).toFixed()] = val === undefined ? 1 : ++val;
  }

  const bucketScore = score => {
    let val = scoreHist[score];
    scoreHist[score] = val === undefined ? 1 : ++val;
  }

  /** Adds a judgement to the data's array indicated by the ID, and
   populates it. Calls to this where the ID already exists can only
   happen on noteFullyCut, so it only updates what's required. */
  const updateJudge = (() => {
    /** Increments the ghost's current score according to ghost data.
     Called within a check for an existing note that isn't a bomb; the
     function shouldn't be called otherwise. */
    function incGhostScore(noteId, stage) {
      const {dist, pre, post, side, dir, mult} = ghost[noteId];
      // The distance score is still saved by the game for errors. Those don't count!
      if (!(dir || side)) return;
      switch (stage) {
        case noteStage.PRE:
          currentGhostScore += mult * (dist + pre);
          break;
        case noteStage.POST:
          currentGhostScore += mult * post;
          break;
      }
    }

    return (nc, time) => {
      const {noteType, finalScore, cutDistanceToCenter, cutDistanceScore, noteID,
        multiplier, initialScore, directionOK, saberTypeOK} = nc;
      let note = data[noteID];
      if (note !== undefined && noteType !== 'Bomb') {
        // It's been shown the pre score isn't guaranteed final until the final score
        // has been determined. It can go higher (or even lower...)
        const newPre = initialScore - cutDistanceScore;
        if (note.pre !== newPre) {
          // console.log(`[+] Note ${nc.noteID} changed from ${note.pre} to ${newPre}`);
          averages[noteType].pre -= note.pre;
          note.pre = newPre;
          averages[noteType].pre += newPre;
        }
        bucketJudge(finalScore);
        bucketScore(finalScore);
        note.post = finalScore - initialScore;
        averages[noteType].post += note.post;
        averages[noteType].notesPost++;
        if (ghost[noteID] !== undefined) incGhostScore(noteID, noteStage.POST);
      } else {
        note = data[noteID] = {
          pre: initialScore - cutDistanceScore,
          dist: cutDistanceScore,
          mult: multiplier,
          type: noteType,
          time: time,
          dir: directionOK,
          side: saberTypeOK
        };
        if (noteType === 'Bomb') return;
        bucketDistance(cutDistanceToCenter);
        averages[noteType].pre += note.pre;
        averages[noteType].dist += note.dist;
        averages[noteType].notesPre++;
        if (ghost[noteID] !== undefined) incGhostScore(noteID, noteStage.PRE);
      }
    }
  })();

  /** Returns the time associated with the requested note ID. */
  const timeFor = (() => {
    return (id) => {
      if (data[id] !== undefined) {
        return data[id].time;
      }
      console.log(`Note ID ${id} doesn't exist`);
      return -1;
    }
  })();

  const currentJudges = (() => judgements)();

  const currentAverages = (() => {
    function populate(type) {
      return {
        pre: averages[type].notesPre === 0 ? 0 : averages[type].pre / averages[type].notesPre,
        post: averages[type].notesPost === 0 ? 0 : averages[type].post / averages[type].notesPost,
        dist: averages[type].notesPre === 0 ? 0 : averages[type].dist / averages[type].notesPre
      }
    }

    return () => ({
      NoteA: populate("NoteA"),
      NoteB: populate("NoteB")
    })
  })();

  const currentScoreAvg = (() => {
    return () => {
      if (data.length === 0) return 0;
      return (
        averages["NoteA"].pre + averages["NoteA"].post + averages["NoteA"].dist +
        averages["NoteB"].pre + averages["NoteB"].post + averages["NoteB"].dist
      ) / (averages["NoteA"].notesPre + averages["NoteB"].notesPre);
    }
  })();

  const updateCombo = (() => {
    const current = () => comboData[comboData.length - 1];
    return (value, songTime = -1) => {
      if (value <= 0) {
        if (current().start !== emptyCombo.start)
          comboData.push({start: emptyCombo.start, end: emptyCombo.end, combo: emptyCombo.combo});
      } else {
        if (current().start === emptyCombo.start) current().start = songTime;
        current().end = songTime;
        current().combo = value;
      }
    }
  })();

  /** Returns the maximum combo attained by iterating through the combo list. */
  const maxCombo = (() => () => {
    let result = 0;
    comboData.forEach(data => { if (data.combo > result) result = data.combo; });
    return result;
  })();

  // TODO: Remove dumps where possible/necessary
  // const dumpData = (() => data)();
  // const dumpAverages = (() => averages)();
  const dumpDistances = (() => distanceDev)();
  const dumpScoreHist = (() => scoreHist)();
  const dumpCombos = (() => comboData)();

  return {
    updateJudge,
    updateCombo,
    timeFor,
    currentJudges,
    currentAverages,
    currentScoreAvg,
    maxCombo,

    dataToB64,
    b64ToData,

    dumpDistances,
    dumpCombos,
    dumpScoreHist,

    numScore,
    updateScore,
    dataLength,

    noteToBinary,
    binaryToNote,

    setGhost,
    ghostExists,
    getGhostScore,

    init() {
      // Don't clear ghost data here because it is set before initialising. Do it in setGhost.
      [data, comboData, distanceDev, scoreHist].forEach(a => a.length = 0);
      comboData.push({start: emptyCombo.start, end: emptyCombo.end, combo: emptyCombo.combo});
      judgements.forEach((_, i) => judgements[i] = 0);
      currentGhostScore = 0;
      for (const side in averages) {
        for (const prop in averages[side]) {
          averages[side][prop] = 0;
        }
      }
    }
  }
})();