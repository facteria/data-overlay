const api = (() => {
  // Indicates to other places in the code to use API features
  const use = true;
  const apiLoc = 'https://localhost:5001/api';

  const request = (method, url, onLoadFunc, jsonPayload) => {
    return new Promise((res) => {
      const xhr = new XMLHttpRequest();
      xhr.open(method, url);
      xhr.onload = () => {
        onLoadFunc(xhr.status);
        res(JSON.parse(xhr.response));
      }
      if (jsonPayload) xhr.setRequestHeader('content-type', 'application/json;charset=UTF-8');
      xhr.send(JSON.stringify(jsonPayload));
    });
  }

  const getSong = (idString) => {
    // ID of a song is a SHA1 hash of a built string, see `events.songStart`
    return request('GET', `${apiLoc}/Song/${idString}`, (status) => {
      switch (status) {
        case 200: break;
        case 404:
          console.log("Came across undiscovered song");
          break;
        default:
          console.log(`Fetching song details returned HTTP code ${status}`);
          break;
      }
    });
  };

  const addSong = (songName, songAuthorName, songBPM, songHash) => {
    return request('POST', `${apiLoc}/Song`, (status) => {
        if (status < 200 || status >= 300)
          console.log(`Error adding song with status ${status}`);
    }, {
      id: songHash,
      name: songName,
      artist: songAuthorName,
      bpm: songBPM
    });
  };

  const getChart = (idString, difficulty) => {
    return request('GET', `${apiLoc}/Chart/${idString}/${getDiffIndex(difficulty)}`, (status) => {
      switch (status) {
        case 200: break;
        case 404:
          console.log("Came across undiscovered chart");
          break;
        default:
          console.log(`Fetching chart details returned HTTP code ${status}`);
          break;
      }
    });
  }

  const addChart = (songId, difficulty, notes, mines, obstacles, maxScore, charter) => {
    return request('POST', `${apiLoc}/Chart`, (status) => {
      if (status !== 201)
        console.log(`Posting chart returned non 201 value, was ${status}`);
    }, {
      songId,
      difficulty: getDiffIndex(difficulty),
      notes,
      mines,
      obstacles,
      maxScore,
      charter
    });
  }

  const addScore = (chartId, perc, ghost, mult) => {
    console.log("Calling addScore");
    return request('POST', `${apiLoc}/Score`, (status) => {
      if (status !== 201)
        console.log(`Errored posting score with status ${status}`);
    }, {
      chartId,
      perc,
      ghost,
      mult: mult * 100
    });
  }

  const getBestGhost = chartId => {
    return request('GET', `${apiLoc}/Score/Best/${chartId}`, (status) => {
      switch (status) {
        case 200: break;
        case 404:
          console.log(`No ghost found for selected chart`);
          break;
      }
    });
  }

  function getDiffIndex(str) {
    switch (str) {
      case 'Easy':        return 0;
      case 'Medium':      return 1;
      case 'Hard':        return 2;
      case 'Expert':      return 3;
      case 'ExpertPlus':  return 4;
    }
  }

  return {
    use,
    getSong,
    addSong,
    getChart,
    addChart,
    addScore,
    getBestGhost
  }
})();