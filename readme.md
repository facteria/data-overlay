# Stats Overlay for Beat Saber

This is a (heavily?) modified fork of [Reselim’s `beat-saber-overlay`](https://github.com/Reselim/beat-saber-overlay) to include more detailed play statistics. It’s a simple web trinity implementation that listens to a web socket hosted by the [HTTP Status](https://github.com/opl-/beatsaber-http-status) plugin.

## Features

![Meta information, score and combo graph](img/meta-score.png)

### Meta and Performance

* **Ghost data**. Shows your, and your personal best’s, current score with comparison to the game. If no ghost data exists, it will only show your accumulated score assuming a PB of 0. Ghost data stores information on scoring values, multiplier, note type, and correctness of direction and saber.
* **Subtractive scoring**. Shows the maximum possible score attainable by taking into account lost points that have incurred.
* **Chart information**. Shows total notes in the chart, total passed, and total hit/avoided. Due to HTTP Status’s limitations, there is not enough information required to correctly determine running totals of obstacles.
* **Time left**. Measured in beats – may not be accurate with variable tempo songs.
* **Score judgements**. Sorts your cuts into buckets. The thresholds are adjustable to your liking, with a few sample presets provided.
* **Separate hand averages**. Exactly like the [Counters+ plugin](https://github.com/Caeden117/CountersPlus/wiki/Cut), shows accuracy and pre/post-cut swing scores for each hand.
* **Numerical score**. The numerical score can be rendered next to the percentage score as shown in the screenshot. By default, this is hidden in `ui.js` at the top of the file (as numerical scores don’t mean much). If you would like to show it anyway, comment out the line.

### Score Graph

* Cuts, split into pre, post and accuracy scores, are displayed as a stacked bar graph.
* The combo section at the top displays areas of the song where combos were attained, highlighting the current max combo. The current running combo is also shown here (hidden once the song has been completed).
* Song progress is shown in the score graph.

![Distance and score histogram, scatter plot, mishits](img/histogram-scatter.png)

### Timing + Histogram

* Timing data for each note is displayed as a scatter plot.
* Full-height bars display missed blocks (red) and incorrect cuts (yellow).
* The left histogram represents cut distance rounded to the nearest centiunit.
* The right histogram represents cut score.

To remind you which ones are which, average values appropriately placed are below:

* ⏺ shows cut distance from the centre of a block.
* ⌛ shows the average timing of hit notes, taking into account negative values rather than tracking positive deviation. This is useful for determining the value required to adjust for the music offset.
* 🏁 shows the average score of all cuts out of 115, which also includes misses.

## Installation

The font used in this layout is Intel’s [Clear Sans](https://01.org/clear-sans), not Positype’s. If you wish to use your own font (of possibly more proportional numerics than this one), keep a note of the negative margins I have applied in the CSS as a quick attempt to resolve vertical centring.

The overlay was designed for use with OBS, though any application that can display the webpage and ideally override CSS will do.

1. Ensure the [Beat Saber HTTP Status](https://github.com/opl-/beatsaber-http-status/releases) plugin has been installed, either through [ModAssistant](https://github.com/Assistant/ModAssistant) or directly.
2. Create a browser source and link to the HTML file.
3. The provided web page was designed to work with size 2560×720 (double the width of 720p), but as of OBS 26.1, one side of the body’s margin must be subtracted; therefore **2536×720** should be the dimensions entered. Scale the source down to fit as required.
4. Ensure the custom CSS does not override any layout properties such as margin or padding. OBS comes with `margin: 0` by default, so this should be removed.
5. Recompose Beat Saber’s cameras if needed, as the provided layout takes up a fair amount of space. Specifically, if the canvas is 16:9, the top left quadrant and the upper half of the top right will be used.

![Video layout example](img/video-example.jpg)

### Ghost Data Backend

In `api.js` the Boolean variable `use` determines whether a backend server that handles saving scores and ghost data should be used.

To summarise, the backend manages a database of songs, charts and scores using an API. My implementation of this in .NET, including a spec, will be uploaded Soon™.

## Modding

Each section has been compartmentalised such that it shouldn’t take too much effort to move the three sections around. Graphs have layers that can be moved between the two if desired. However, the values in stylesheets and areas where sizes must be specified (e.g. graphs) was determined around the 2560×720 layout.

Settings for graphs such as the size of elements to be drawn can be set in the respective Javascript file.

## Future Work & Notes

* [Investigate] Beat timer’s offset is incorrect since the entire length of the song is used rather than the chart. In-game plugins correctly determine note timing, so there should be a way.
* [Issue] Since Beat Saber upgraded to 1.13.2, the `finished` message is no longer sent when a stage has been completed. Now, only `menu` is called. Unfortunately, `menu` doesn’t store performance data, so various parts of the code have been modified to store those values. It’s assumed that this will eventually be fixed, so `FIXME` annotations have been added reminding to revert the changes.