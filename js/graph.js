const graph = (() => {
  let scoreCanvas = document.getElementById("score-graph");
  let scoreCtx = scoreCanvas.getContext("2d");
  let errorCanvas = document.getElementById("error-graph");
  let errorCtx = errorCanvas.getContext("2d");
  let scatterCanvas = document.getElementById("scatter-graph");
  let scatterCtx = scatterCanvas.getContext("2d");
  let histCanvas = document.getElementById("hist-graph");
  let histCtx = histCanvas.getContext("2d");

  const width = scoreCanvas.width;
  const height = scoreCanvas.height;
  // As the canvas can scale when resized and constant pixels won't work
  // smoothly with CSS, the bar height needs to be a percentage instead
  const comboBarHeight = height * 0.1;
  const maxScoreHeight = height - comboBarHeight;

  const colRed = "#f33";
  const colBlue = "#29f";
  const colGreen = "#4e4";
  const colPurple = "#b4e";
  const colYellow = "#cc1";
  const colCombo = "#58c";
  const colComboMax = "#870";

  scoreCtx.font = "500 32px Clear Sans";
  scoreCtx.textAlign = "center";
  scoreCtx.textBaseline = "middle";

  // Width of bars/dots to draw on the graph
  const barWidth = 3;
  const errorWidth = 3;
  const plotSize = 6;
  // Maximum value of the scatter plot (+/-)
  const maxYPlot = 100;

  const drawRes = (progress, pre, post, acc, mistake = false) => {
    let x = progress * width - barWidth / 2;

    if (pre === 0 && post === 0 && acc === 0) {
      x = progress * width - errorWidth / 2;
      errorCtx.fillStyle = mistake ? colYellow : colRed;
      // If placed on score graph: accounts for combo bar height
      // errorCtx.fillRect(x, comboBarHeight, errorWidth, maxScoreHeight);
      // If placed on histogram: takes up the full height instead
      errorCtx.fillRect(x, 0, errorWidth, height);
    }

    if (pre < 0 || pre > 70 || post < 0 || post > 30 || acc < 0 || acc > 15) {
      console.log(`Got invalid score data: ${pre} ${post} ${acc}`);
      // return;
      // BUG: Sometimes (maybe when it lags, not sure) scores exceed 115.
    }

    const preHeight = pre / 115 * maxScoreHeight;
    const postHeight = post / 115 * maxScoreHeight;
    const accHeight = acc / 115 * maxScoreHeight;

    // Start with the height of all three bars stacked together
    let curHeight = preHeight + postHeight + accHeight;

    // Accuracy
    scoreCtx.fillStyle = colGreen;
    scoreCtx.fillRect(x, height - curHeight, barWidth, curHeight);

    // After
    curHeight -= accHeight;
    scoreCtx.fillStyle = colBlue;
    scoreCtx.fillRect(x, height - curHeight, barWidth, curHeight);

    // Before
    curHeight -= postHeight;
    scoreCtx.fillStyle = colPurple;
    scoreCtx.fillRect(x, height - curHeight, barWidth, curHeight);
  };

  const drawCombo = (fromPerc, toPerc, combo, isMax, showCurrent) => {
    const x = fromPerc * width;
    const comboWidth = toPerc * width - x;
    scoreCtx.fillStyle = isMax ? colComboMax : colCombo;
    scoreCtx.fillRect(x, 0, comboWidth, comboBarHeight);
    if (isMax || showCurrent) {
      scoreCtx.fillStyle = "white";
      scoreCtx.fillText(combo, x + comboWidth / 2, comboBarHeight / 2 + 6);
    }
  };

  const drawCombos = (dataTable, songDuration, maxCombo, songFinished = false) => {
    // There is no guarantee the combo number won't exceed the width
    // of a combo bar (e.g. longer songs) so clear it just to be safe
    scoreCtx.clearRect(0, 0, width, comboBarHeight);
    dataTable.forEach((combo, i) => {
      drawCombo(
        combo.start / songDuration,
        combo.end / songDuration,
        combo.combo,
        combo.combo === maxCombo,
        // Hide the current combo when song is finished
        i === dataTable.length - 1 && !songFinished);
    });
  };

  const drawPlot = (perc, value, colour) => {
    const x = perc * width - plotSize / 2;
    scatterCtx.fillStyle = colour;
    scatterCtx.fillRect(x, (value / 2 / maxYPlot + 0.5) * height, plotSize, plotSize);
  };

  // Iterates through an array of frequencies to draw. Assumes histogram has
  // no negative values since we are using array indices for them.
  const drawHist = (values, colour, maxValue = 115, startFrom = 1, clear = true) => {
    let modeFreq = 0;
    // Determine the mode's value that will render a full bar, other ones scale
    values.forEach(i => modeFreq = i > modeFreq ? i : modeFreq);
    const barWidth = width / (maxValue + 1 - startFrom);
    // We may not want to clear the canvas (e.g. stacking two histograms)
    if (clear) histCtx.clearRect(0, 0, width, height);
    histCtx.fillStyle = colour;
    for (let i = values.length - 1; i >= startFrom; i--) {
      const x = (i - startFrom) * barWidth;
      const barHeight = values[i] / modeFreq * height;
      histCtx.fillRect(x, height - barHeight, barWidth, barHeight);
    }
  };

  const init = (() => {
    function drawCentreLine(ctx, colour = "#666", strokeWidth = 1) {
      ctx.beginPath();
      ctx.strokeStyle = colour;
      ctx.lineWidth = strokeWidth;
      ctx.moveTo(0, height / 2);
      ctx.lineTo(width, height / 2);
      ctx.stroke();
    }

    return () => {
      [scoreCtx, errorCtx, scatterCtx, histCtx].forEach(ctx => {
        ctx.clearRect(0, 0, width, height);
      });
      drawCentreLine(scatterCtx);
    }
  })();

  return {
    drawRes,
    drawCombos,
    drawPlot,
    drawHist,
    init
  }
})();

graph.init();