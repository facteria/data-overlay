function main() {
    const query = new URLSearchParams(location.search);
    const ip = query.get("ip") || "localhost";
    const port = query.get("port") || 6557;

    const socket = new WebSocket(`ws://${ip}:${port}/socket`);

    socket.addEventListener("open", () => {
        console.log("WebSocket opened");
    });

    socket.addEventListener("message", (message) => {
        const data = JSON.parse(message.data);

        // Look in the events const for an appropriately named event
        const event = events[data.event];

        if (event) {
            event(data);
        }
    });

    socket.addEventListener("close", () => {
        console.log("Failed to connect to server, retrying in 5 seconds");
        setTimeout(main, 5000);
    });
}
main();