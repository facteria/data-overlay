const events = {

  // Sent when client connects to the web socket server.
  // If it hooks in mid-game, grab the stats.
  hello(data) {
    console.log("Connected to Beat Saber");
    const {performance, beatmap} = data.status;
    if (beatmap && performance) {
      events.songStart(data);
      if (beatmap.paused) events.pause(data);
    }
  },

  songStart(data) {
    const {performance, beatmap, mod} = data.status;
    const {songName, songAuthorName, songBPM, levelAuthorName, difficulty, notesCount, bombsCount, obstaclesCount, maxScore} = beatmap;
    const {multiplier} = mod;
    let apiChartId = -1;
    let songId = "";

    function start() {
      ui.chartBegin(beatmap, apiChartId, multiplier);
      ui.setTotalNotes(notesCount + bombsCount);
      ui.performance(performance);
      if (beatmap.paused) events.pause(data);
      // ui.show();
    }

    if (api.use) {
      function findChart(chartResult) {
        let result = null;
        chartResult.forEach(chart => {
          if (
            chart.notes === notesCount &&
            chart.mines === bombsCount &&
            chart.obstacles === obstaclesCount
          ) result = chart;
        });
        return result;
      }

      hash.getHash(`${songName.length}:${songName}|${songAuthorName.length}:${songAuthorName}|${songBPM}`)
        .then(hash => {
          songId = hash;
          return api.getSong(hash);
        })
        .then(songResult => {
          // Find song and add it to the database if it's undiscovered. If it is, then call getChart
          // only once it's done adding. Otherwise, just call getChart immediately.
          if (songResult.status === 404) return api.addSong(songName, songAuthorName, songBPM, songId);
          else return songResult;
        })
        .then(() => {
          return api.getChart(songId, difficulty);
        })
        .then(chartResult => {
          // Do the same thing as above, but with a chart.
          if (chartResult.status === 404 || (chartResult = findChart(chartResult)) === null)
            return api.addChart(songId, difficulty, notesCount, bombsCount, obstaclesCount,
              Math.round(maxScore / multiplier), levelAuthorName);
          else return chartResult;
        })
        .then(chart => {
          if (chart == null) console.log("Something's not right, we should have a specific chart by now");
          else {
            apiChartId = chart.id;
            return api.getBestGhost(apiChartId);
          }
        })
        .then(bestGhostResult => {
          if (bestGhostResult.status !== 404) {
            const {ghost} = bestGhostResult;
            return playdata.setGhost(ghost);
          }
        })
        .then(start)
        .catch(error => console.error(error));
    } else
      start();
  },

  finished(data) {
    const {performance} = data.status;
    ui.finished(performance);
  },

  // FIXME: Remove finished call when finished() has been patched up to work
  // 26/1/21 - Rather than finished(), menu() is now called when a song
  // finishes. Unfortunately, the menu event doesn't hold performance
  // information, so we'll need to rely on other places.
  menu() {
    ui.finished(null);

    ui.timer.stop();
    // ui.hide();
  },

  failed(data) {
    // TODO: Halt tracking and colour texts red to denote failing
    // Contains only data.status.performance
  },

  // When note has just been cut (i.e. no post-swing). Time is in ms.
  // Bomb cuts don't trigger this function and are bombCut() exclusive.
  noteCut(data) {
    const {timeDeviation} = data.noteCut;
    const {performance} = data.status;
    const {combo: currentCombo} = performance;

    let time = data.time - (timeDeviation * 1000).toFixed();
    const songTime = time - ui.timer.getBegan();
    playdata.updateJudge(data.noteCut, songTime);
    playdata.updateCombo(currentCombo, songTime);
    ui.noteCut(data.noteCut, performance, time);
  },

  // Note cuts shouldn't update scores since there is no guarantee that
  // each note will be in order. Combos and scores can then go backwards.
  // Leave updating scores to scoreChanged().
  noteFullyCut(data) {
    const {noteID} = data.noteCut;
    let time = playdata.timeFor(noteID);
    playdata.updateJudge(data.noteCut, time);
    ui.noteFullyCut(data.noteCut, time + ui.timer.getBegan());
  },

  obstacleEnter(data) {
    ui.performance(data.status.performance);
  },
  obstacleExit(data) {
    ui.performance(data.status.performance);
  },

  noteMissed(data) {
    eventsUtils.updatePerf(data, true);
  },

  // Assumes a bomb cut is data-wise the same as a note miss. Only
  // difference that might occur is that the graph representation
  // should be yellow rather than a red?
  bombCut(data) {
    eventsUtils.updatePerf(data, true);
  },

  bombMissed(data) {
    eventsUtils.updatePerf(data, false);
  },

  // Update the score as soon as possible. Only contains performance
  scoreChanged(data) {
    const {score} = data.status.performance;
    ui.performance(data.status.performance);
    playdata.updateScore(score);
  },

  pause(data) {
    const {beatmap} = data.status;
    const {paused} = beatmap;
    ui.timer.pause(paused + (Date.now() - data.time));
  },

  resume(data) {
    const {beatmap} = data.status;
    const {length, songBPM, songTimeOffset} = beatmap;
    ui.timer.start(
      ui.timer.getBegan() + (Date.now() - data.time),
      length,
      songBPM,
      songTimeOffset
    );
  }
};

const eventsUtils = (() => {
  const updatePerf = (() => {
    return (data, noteMissed) => {
      const {timeDeviation} = data.noteCut;
      const time = data.time - (timeDeviation * 1000).toFixed();
      playdata.updateJudge(data.noteCut, time - ui.timer.getBegan());
      ui.performance(data.status.performance);

      if (noteMissed) {
        playdata.updateCombo(0);
        ui.noteMissed(data.noteCut, time);
      }
    }
  })();

  return { updatePerf }
})();