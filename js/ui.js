const ui = (() => {
  // Constant scoring systems aren't particularly useful, when compared with
  // percentage-based scores. Uncomment to show it next to the percentage.
  // document.getElementById("current-score").style.display = "none";

  let apiChart = -1;
  let maxScore = -1;
  let songLength = 0;
  let totalTimingDev = 0;
  let totalDistDev = 0;
  let mult = 1;

  // FIXME: Caching for menu() use. Remove when finished() works.
  let totalNotes = 0;
  const setTotalNotes = (() => notes => { totalNotes = notes })();

  const cover = document.getElementById("cover-image");
  const title = document.getElementById("song-name");
  const subtitle = document.getElementById("subtitle");
  const bpm = document.getElementById("bpm");
  const artist = document.getElementById("artist");
  const charter = document.getElementById("charter");
  const difficulty = document.getElementById("difficulty");
  const notesCount = document.querySelector("#notes-total .count");
  const minesCount = document.querySelector("#mines-total .count");
  const obstaclesCount = document.querySelector("#obstacles-total .count");
  const timerSongLength = document.querySelector("#timer .count");

  const currentPerc = document.getElementById("current-perc");
  const currentScore = document.getElementById("current-score");
  const pbPerc = document.getElementById("pb-perc");
  const maxPerc = document.getElementById("max-perc");
  const pbPercDiff = document.getElementById("pb-perc-diff");
  const maxPercDiff = document.getElementById("max-perc-diff");
  const notesHit = document.querySelector("#notes-progress .pri-num");
  const notesPassed = document.querySelector("#notes-progress .sec-num");
  const minesMissed = document.querySelector("#mines-progress .pri-num");
  const minesPassed = document.querySelector("#mines-progress .sec-num");
  const obstaclesMissed = document.querySelector("#obstacles-progress .pri-num");
  const timerPassedPri = document.querySelector("#timer-progress .pri-num");
  const timerPassedSec = document.querySelector("#timer-progress .sec-num");
  const avgCut = document.querySelector("#avg-cut .value");

  // Number of decimal points to round to for left/right cut averages
  const avgCutsDecimals = 1;
  const avgCutsLeft = document.querySelectorAll("#averages .avg-row")[0].children;
  const avgCutsRight = document.querySelectorAll("#averages .avg-row")[1].children;

  const judges = [
    document.getElementById("fan"),
    document.getElementById("exc"),
    document.getElementById("grt"),
    document.getElementById("dec"),
    document.getElementById("wof"),
    document.getElementById("mis")
  ];

  const timingDev = document.querySelector("#avg-timing .value");
  const centreDev = document.querySelector("#avg-distance .value");

  const roundFunc = { FLOOR: Math.floor, CEIL: Math.ceil, ROUND: Math.round };
  Object.freeze(roundFunc);
  const perc = (value, dp, mathFunc = roundFunc.FLOOR) =>
    (mathFunc(value * Math.pow(10, dp + 2)) / Math.pow(10, dp)).toFixed(dp);

  const performance = (() => {
    return (data) => {
      const {hitNotes, hitBombs, currentMaxScore, score, passedNotes, passedBombs} = data;
      currentPerc.innerText = currentMaxScore > 0 ?
        perc(score / currentMaxScore, 2) :
        "0.00";
      currentScore.innerText = score;

      if (currentMaxScore > 0 && maxScore > 0) {
        printPbValues(score / maxScore, playdata.getGhostScore() / maxScore);
        printSubtractiveValues((maxScore - (currentMaxScore - score)) / maxScore);
      }

      notesHit.innerText = hitNotes;
      notesPassed.innerText = passedNotes;
      minesMissed.innerText = passedBombs - hitBombs;
      minesPassed.innerText = passedBombs;
    }
  })();

  const printPbValues = (() => (scorePerc, ghostPerc) => {
    if (scorePerc - ghostPerc < 0 && pbPercDiff.classList.contains("diff-pos") ||
      scorePerc - ghostPerc > 0 && pbPercDiff.classList.contains("diff-neg")) {
      ["diff-pos", "diff-neg"].forEach(cssClass => pbPercDiff.classList.toggle(cssClass));
    }
    pbPerc.innerText = `${perc(scorePerc, 2)}${playdata.ghostExists() ? ' / ' + perc(ghostPerc, 2) : ""}`;
    pbPercDiff.innerText = `${(scorePerc - ghostPerc < 0 ? "–" : "+")}${perc(Math.abs(scorePerc - ghostPerc), 2, roundFunc.FLOOR)}`;
  })();

  const printSubtractiveValues = (() => maxAttainablePerc => {
    maxPerc.innerText = perc(maxAttainablePerc, 2);
    maxPercDiff.innerText = "–" + perc(1 - maxAttainablePerc, 2, roundFunc.CEIL);
  })();

  const updateHist = (() => {
    return () => {
      graph.drawHist(playdata.dumpScoreHist, "#282", 115, 1, true);
      graph.drawHist(playdata.dumpDistances, "#278", 115, 0, false);
    }
  })();

  // Updates what is determined to be final at this point. There will
  // be another call from noteFullyCut soon for post-swing scores.
  const noteCut = (() => {
    return (nc, perf, time) => {
      // Hit/missed notes are updated in performance(), don't do it here
      const {cutDistanceToCenter, initialScore, timeDeviation, cutDistanceScore} = nc;
      const {maxCombo, hitNotes} = perf;
      totalTimingDev += timeDeviation * 1000;
      totalDistDev += cutDistanceToCenter;
      timingDev.innerText = (totalTimingDev / hitNotes).toFixed(2) + " ms";
      centreDev.innerText = (totalDistDev * 100 / hitNotes).toFixed(2) + " cu";
      graph.drawRes(ui.timer.currentPerc(time), initialScore - cutDistanceScore, 0, cutDistanceScore);
      // TODO: Possible decoupling opportunity (data, graph and UI)
      graph.drawCombos(playdata.dumpCombos, ui.timer.getSongDuration(), maxCombo);
      // TODO: Determine appropriate timing thresholds/maximum values for colours
      graph.drawPlot(ui.timer.currentPerc(time), timeDeviation * 1000, "#fff");
      updateHist();
      updateAverages();
    }
  })();

  const noteFullyCut = (() => {
    // TODO: Currently assumes there are six judges. Generalise it.
    function updateJudges() {
      let table = playdata.currentJudges;
      judges.forEach((el, i) => el.innerText = table[i]);
    }

    return ({cutDistanceScore, finalScore, initialScore}, time) => {
      graph.drawRes(
        ui.timer.currentPerc(time),
        initialScore - cutDistanceScore,
        finalScore - initialScore,
        cutDistanceScore
      );
      updateJudges();
      updateHist();
      updateAverages();
      avgCut.innerText = playdata.currentScoreAvg().toFixed(avgCutsDecimals);
    }
  })();

  const noteMissed = (() => {
    return ({saberType}, time) => {
      graph.drawRes(
        ui.timer.currentPerc(time), 0, 0, 0,
        saberType != null
      );
    }
  })();

  const timer = (() => {
    let bar = document.querySelector("#song-progress");
    let active = false;
    let began, duration, songOffset, bpm;

    function format(time) {
      let minutes = Math.floor(time / 60);
      let seconds = time % 60;
      if (seconds < 10) seconds = "0" + seconds;
      return `${minutes}:${seconds}`;
    }

    // Called every frame when the game is running (i.e. not paused).
    // Time is updated to either what a message specified, or now.
    function update(time) {
      time = time || Date.now();
      const elapsed = time - began;
      // const progress = Math.floor(elapsed / 1000);
      setTime(Math.min(elapsed / duration, 1));
      const beats = Math.ceil((duration - elapsed - songOffset) / 60000 * bpm);
      timerPassedPri.innerText = Math.max(Math.floor(beats / 4), 0);
      timerPassedSec.innerText = beats > 0 ? beats % 4 : 0;
    }

    // Sets the bar's width based on the percentage provided
    function setTime(perc) {
      bar.setAttribute("style", `width: ${perc * 100}%`);
    }

    function loop() {
      if (active) {
        update();
        requestAnimationFrame(loop);
      }
    }

    // Returns the percentage of the song with respect to real life time.
    function currentPerc(time) {
      return (time - began) / duration;
    }

    function getBegan() {
      return began;
    }

    function getSongDuration() {
      return duration;
    }

    return {
      start(time, length, songBpm, offset) {
        active = true;
        began = time;
        bpm = songBpm;
        songOffset = offset;
        duration = length;
        loop();
      },

      pause(time) {
        active = false;
        update(time);
      },

      stop() {
        active = false;
        began = undefined;
        duration = undefined;
      },

      getBegan,
      getSongDuration,
      currentPerc
    }
  })();

  const chartBegin = (() => {
    function format(number) {
      if (Number.isNaN(number)) return "NaN";
      if (Math.floor(number) !== number) return number.toFixed(2);
      return number.toString();
    }

    function setDiffSquare(diff, value = -1) {
      let text;
      switch (diff) {
        case "Easy":
          text = "E";
          break;
        case "Normal":
          text = "N";
          break;
        case "Hard":
          text = "H";
          break;
        case "Expert":
          text = "X";
          break;
        case "ExpertPlus":
          text = "+";
          break;
        default:
          text = "?";
          break;
      }
      for (let i = difficulty.classList.length - 1; i >= 0; i--) {
        const c = difficulty.classList[i];
        if (c.startsWith('diff-')) difficulty.classList.remove(c);
      }
      difficulty.classList.add(`diff-${diff.toLowerCase()}`);
      difficulty.innerText = value !== -1 ? value : text;
    }

    return (data, apiChartId = -1, multiplier = 1) => {
      graph.init();
      playdata.init();
      reset();
      apiChart = apiChartId;
      mult = multiplier;

      const {
        songCover, length, songBPM, notesCount: nCount, difficulty: diff, obstaclesCount: obstacles, songName,
        songTimeOffset, maxScore: max, songSubName, songAuthorName, levelAuthorName, bombsCount
      } = data;

      cover.setAttribute("src", `data:image/png;base64,${songCover}`);
      title.innerText = songName;
      subtitle.innerText = songSubName;
      artist.innerText = songAuthorName;
      charter.innerText = levelAuthorName;
      setDiffSquare(diff);
      bpm.innerText = songBPM;
      notesCount.innerText = format(nCount);
      minesCount.innerText = bombsCount;
      obstaclesCount.innerText = obstacles;
      obstaclesMissed.innerText = obstacles;
      // Currently the system broadcasts obstacles hit, but has no ID associated
      // with it. The strategy to decrement on obstacleEnter isn't reliable
      // because it's possible to enter and exit the same obstacle more than once.
      obstaclesMissed.style.display = "none";

      const totalBeats = Math.floor((length - songTimeOffset) / 60000 * songBPM);
      timerSongLength.innerText = `${Math.floor(totalBeats / 4)}:${totalBeats % 4}`;

      songLength = length;
      maxScore = max;

      timer.start(Date.now(), length, songBPM, songTimeOffset);
    }
  })();

  const finished = (() => {
    // FIXME: Revert changes when finished() is actually called
    // Makes life a bit easier to grab the data straight from the source, but
    // requires having perf from a message. menu(), for instance, doesn't have that.
    // Also, menu() can be called when the player wants to restart the song, so a
    // check is needed to make sure those scores aren't being submitted.
    return (perf) => {
      // const {maxCombo, score} = perf;
      if (api.use && playdata.dataLength() === totalNotes) {
        api.addScore(apiChart, playdata.numScore() / maxScore, playdata.dataToB64(), mult)
          .then()
          .catch();
        // api.addScore(apiChart, score / maxScore, playdata.dataToB64(), mult);
      }
      // TODO: Decoupling function as per in noteHit()
      graph.drawCombos(playdata.dumpCombos, ui.timer.getSongDuration(), playdata.maxCombo, true);
      // graph.drawCombos(playdata.dumpCombos, ui.timer.getSongDuration(), maxCombo, true);
    }
  })();

  const reset = (() => {
    return () => {
      currentPerc.innerText = "0.00";
      currentScore.innerText = "0";
      printPbValues(0, 0);
      printSubtractiveValues(1);
      notesHit.innerText = 0;
      notesPassed.innerText = 0;
      minesMissed.innerText = 0;
      minesPassed.innerText = 0;
      obstaclesMissed.innerText = "";
      timerPassedPri.innerText = 0;
      timerPassedSec.innerText = 0;

      avgCut.innerText = "0.0";

      judges.forEach(el => el.innerText = 0);
      [avgCutsLeft, avgCutsRight].forEach(arr => {
        for (let i = arr.length - 2; i >= 0; i--) {
          arr[i].innerText = (0).toFixed(avgCutsDecimals);
        }
      });

      totalTimingDev = 0;
      totalDistDev = 0;
      timingDev.innerText = "0.00 ms";
      centreDev.innerText = "0.00 cu";
    }
  })();

  const init = (() => {
    reset();
    title.innerText = "Title";
    subtitle.innerText = "Subtitle";
    bpm.innerText = "BPM";
    artist.innerText = "Artist";
    charter.innerText = "Official";
    difficulty.innerText = "?";
    notesCount.innerText = "Notes";
    minesCount.innerText = "Mines";
    obstaclesCount.innerText = "Walls";
    timerSongLength.innerText = "Time";
    graph.init();
  })();

  const updateAverages = (() => {
    function updateSide(elArray, sideObj) {
      elArray[0].innerText = sideObj.pre.toFixed(avgCutsDecimals);
      elArray[1].innerText = sideObj.post.toFixed(avgCutsDecimals);
      elArray[2].innerText = sideObj.dist.toFixed(avgCutsDecimals);
    }

    return () => {
      const averages = playdata.currentAverages();
      updateSide(avgCutsLeft, averages["NoteA"]);
      updateSide(avgCutsRight, averages["NoteB"]);
    }
  })();

  return {
    performance,
    noteCut,
    noteFullyCut,
    noteMissed,
    finished,
    timer,
    chartBegin,
    setTotalNotes
  }
})();