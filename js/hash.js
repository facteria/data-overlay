const hash = (() => {

  // Usage (remember it's async!)
  // hash.getHash("abc").then(r => console.log(r));
  const getHash = async (str) => {
    async function digestMessage(message) {
      const msgUint8 = new TextEncoder().encode(message);
      const hashBuffer = await crypto.subtle.digest('SHA-1', msgUint8);
      const hashArray = Array.from(new Uint8Array(hashBuffer));                   // convert buffer to byte array
      return hashArray.map(b => b.toString(16).padStart(2, '0')).join('');   // convert bytes to hex string
    }
    return await digestMessage(str);
  };

  return {
    getHash
  }

})();